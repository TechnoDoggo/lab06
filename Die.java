import java.util.Random;

public class Die
{
	private int pips;
	private Random chance;
	
	public Die()
	{
		this.pips = 1;
		this.chance = new Random();
	}
	
	public int getPips()
	{
		return this.pips;
	}
	
	public void roll()
	{
		this.pips = chance.nextInt(6) + 1;
	}
	
	public String toString()
	{
		return "You rolled a " + this.pips + "!";
	}
}
	