public class Board
{
	private Die redDice;
	private Die blackDice;
	private boolean[] closedTiles;
	
	public Board() 
	{
		this.redDice = new Die();
		this.blackDice = new Die();
		this.closedTiles = new boolean[12];
		for (int i = 0; i < closedTiles.length; i++)
		{
			closedTiles[i] = false;
		}	
	}
	
	public boolean playATurn()
	{
		boolean alreadyRolled = false;
		redDice.roll();
		blackDice.roll();
		
		int sumRolled = redDice.getPips() + blackDice.getPips();
		
		if(closedTiles[sumRolled - 1])
		{
			System.out.println("Uh oh. Looks like the tile at position " + sumRolled + " has already been closed. Sorry about that...");
			alreadyRolled = true;
		}
		else
		{
			System.out.println("Oh hey. The tile at position " + sumRolled + " hasn't been closed yet. Good for you. We're closing the tile at position " + sumRolled + " though, so don't get too happy");
			closedTiles[sumRolled - 1] = true;
		}
		return alreadyRolled;
	}
	
	public String toString()
	{
		String currentGameStatus = " ";
		for (int i = 0; i < closedTiles.length; i++)
		{
			if (closedTiles[i])
			{
				currentGameStatus = (currentGameStatus + "X ");
			}
			else
			{
				String nonRolledNumber = String.valueOf(i + 1);
				currentGameStatus = (currentGameStatus + nonRolledNumber + " ");
			}
		}
		return currentGameStatus;
	}
}