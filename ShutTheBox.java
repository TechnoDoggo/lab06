import java.util.Date;

public class ShutTheBox
{
	public static void main (String[] args) throws InterruptedException
	{
		java.util.Scanner reader = new
		java.util.Scanner(System.in);

		System.out.println("Hello there. Welcome to the Shut The Box virtual game. Before we get started however, could you please give me your names?");
		Thread.sleep(2000);
		System.out.println("Starting with Player 1, what is your name?");
		String player1Name = reader.nextLine();
		Thread.sleep(2000);
		System.out.println("And now Player 2, what is your name?");
		String player2Name = reader.nextLine();
		Thread.sleep(2000);
		System.out.println("Well then. Hello to you both, " + player1Name + " and " + player2Name + ", and we wish that you both have fun. Good luck.");
		
		Board GameBoard = new Board();
		boolean gameOver = false;
		
		while(!gameOver)
		{
			Thread.sleep(2000);
			System.out.println(player1Name + "'s Turn.");
			System.out.println(GameBoard);
			if(GameBoard.playATurn())
			{
				Thread.sleep(1500);
				System.out.println(player2Name + " wins.");
				gameOver = true;
				break;
			}
			Thread.sleep(2000);
			System.out.println(player2Name + "'s Turn.");
			System.out.println(GameBoard);
			if(GameBoard.playATurn())
			{
				Thread.sleep(1500);
				System.out.println(player1Name + " wins.");
				gameOver = true;
			}
		}
	}
}